FROM quay.io/keycloak/keycloak:23.0.3-0 as builder

ENV KC_HEALTH_ENABLED=true
ENV KC_METRICS_ENABLED=true
ENV KC_FEATURES=token-exchange
ENV KC_DB=postgres

# builder is currently not necessary, but kept for future use/reference

FROM quay.io/keycloak/keycloak:23.0.3-0

# COPY --from=builder /opt/keycloak/ /opt/keycloak/

WORKDIR /opt/keycloak

ENV KEYCLOAK_ADMIN=admin
ENV KEYCLOAK_ADMIN_PASSWORD=admin
ENV KC_HOSTNAME=localhost
ENV KC_HOSTNAME_STRICT_HTTPS=false
ENV PROXY_ADDRESS_FORWARDING=true

# proxy edge: ssl-termination by reverse-proxy, http between reverse proxy and keycloak
# client <-https-> reverse proxy (nginx) <-http-> keycloak
ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start", "--proxy", "edge"]
